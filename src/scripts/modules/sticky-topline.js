export default function toplineSticky() {

  const scroll_height = 88,
    bootstrap_navbar = document.querySelector(".topmnu--line");

  const scrollIt = () => {
    if (window.scrollY > scroll_height) {
      bootstrap_navbar.classList.add("fixed-top");
    } else {
      bootstrap_navbar.classList.remove("fixed-top");
    }
  };

  window.addEventListener("scroll", scrollIt);
}