import 'bootstrap';
import '../styles/app.sass';

// Modules
import toplineSticky from '../scripts/modules/sticky-topline';
import scrollToSection from '../scripts/modules/scroll-toSection';
import '../scripts/modules/reviews-videos';
import '../scripts/modules/partners';

// init
toplineSticky();
scrollToSection();

